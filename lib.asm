global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_string_stderr

%define STD_IN 0
%define STD_OUT 1
%define STD_ERR 2
%define SYS_EXIT 60
%define SYS_READ 0
%define SYS_WRITE 1
%define SPACE 0x20
%define TAB 0x9
%define LINE 0xA

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
    cmp byte[rdi, rax], 0
    jz .length_end
    inc rax
    jmp .counter
    .length_end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi

    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdx, 1
    mov rsi, rsp
    mov rdi, STD_OUT
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LINE
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push r12
	sub rsp, 32
	mov r12, 20		
	mov rax, rdi
	mov rsi, 10
	mov byte [rsp+r12], 0
	
	.loop:
	xor rdx, rdx
	div rsi ;div always divaite rax, and remainder to rdx
	add rdx, '0'
	dec r12
	mov [rsp+r12], dl
	test rax, rax
	jnz .loop

	lea rdi, [rsp+r12]
	call print_string
	add rsp, 32
	pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jl .negative
    jmp print_uint
    .negative:
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor rax, rax
    .loop:
    mov al, [rdi+rcx]
    cmp al, [rsi+rcx]
    jne .error
    inc rcx
    test al, al
    jnz .loop
    mov rax, 1
    ret
.error:
    xor rax, rax
    ret

    ; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax,rax
    sub rsp, 16;mem
    mov rdi, STD_IN
    mov rsi, rsp
    xor rdx, rdx
    add rdx, 1
    syscall
    test rax, rax
    jz .ret
    xor	rax, rax
    mov	al, byte[rsp]
    .ret:
    add rsp, 16
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14

    xor r12, r12 ;counter
    mov r13, rdi ;address
    mov r14, rsi ;length
    .loop:
    call read_char
    cmp al, 0x20
    je .loop
    cmp al, 0x9
    je .loop
    cmp al, 0xA
    je .loop
    .loop_2: 
    cmp r12, r14
    je .fail
    test al, al
    je .successful
    cmp al, 0x20
    je .successful
    cmp al, 0x9
    je .successful
    cmp al, 0xA
    je .successful
    mov [r13+r12], al
    inc r12
    call read_char
    jmp .loop_2
    .fail:
    xor rax, rax
    jmp .done
    .successful:
    mov rax, r13
    mov byte [r13+r12], 0
    mov rdx, r12
    .done:
    pop r14
    pop r13
    pop r12
    ret


    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    test rdi, rdi
    jz .end
    .loop:
	movzx rcx, byte [rdi]
	sub cl, '0'
	cmp cl, 9
	ja .end
	imul rax, rax, 0xA
	add rax, rcx
	inc rdi
	inc rdx
	jmp .loop
    .end:
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov sil, [rdi]
    cmp sil, '+'
    je .signed
    cmp sil, '-'
    jne parse_uint
    .signed:
    push rdi
    inc rdi
    call parse_uint
    pop rdi
    test rdx, rdx
    jz .done
    inc rdx
    cmp byte [rdi], '-'
    jne .done
    neg rax
    .done:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
    mov cl, [rdi+rax]
    mov [rsi+rax], cl
    inc rax
    cmp rax, rdx
    jge .fail
    test cl, cl
    jnz .loop 
    jmp .ret
    .fail:
    xor rax, rax
    .ret:
    ret

print_string_stderr:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi

    mov rax, SYS_WRITE
    mov rdi, STD_ERR
    syscall
    ret


ASM = nasm
ASM_FLAGS = -felf64
PYTHON = python3

program: main.o lib.o dict.o
	ld -o $@ $^

main.o: main.asm words.inc lib.inc dict.inc

words.inc: colon.inc

dict.o: dict.asm lib.inc

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

clean:
	rm *.o

test:
	$(PYTHON) test.py

.PHONY: clean test

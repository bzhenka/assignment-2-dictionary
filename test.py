import subprocess

# Функция для запуска программы и сравнения вывода с ожидаемым результатом
def run_program(input_text):
    try:
        # Запуск программы и передача ввода
        process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout, stderr = process.communicate(input_text)
        return process.returncode, stdout, stderr
    except Exception as e:
        return -1, str(e), ""

# Тест 1: Проверка корректной работы программы с валидным вводом
input_text = "first_word"
expected_output = "first word explanation"
return_code, stdout, stderr = run_program(input_text)

if return_code == 0 and stdout.strip() == expected_output:
    print("Тест 1 прошел успешно")
else:
    print("Тест 1 провален")

# Тест 2: Проверка работы программы с некорректным вводом
input_text = "no_word"
return_code, stdout, stderr = run_program(input_text)

if return_code != 0 and "error, not find word in text" in stderr:
    print("Тест 2 прошел успешно")
else:
    print("Тест 2 провален")

# Тест 3: Проверка работы программы с некорректным вводом
input_text = "Loremipsumdolorsitamet,consecteturadipiscingelitsedauctorsemineleifendblandit.Nullamviverraelitvitaeleopharetra,idrhoncusnullavehicula.Maecenasutliberonecmassasagittissollicitudinnecidtellus.Innonaugueetpurusfermentumullamcorper.Sedauctorsemineleifendbland"
return_code, stdout, stderr = run_program(input_text)

if return_code != 0 and "error, because very long word" in stderr:
    print("Тест 3 прошел успешно")
else:
    print("Тест 3 провален")

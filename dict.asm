%include "lib.inc"
global find_word
%define SHIFT 8
section .text
find_word:
    push r12
    push r13
    mov r12, rsi
    mov r13, rdi
.loop:
    add r12, SHIFT
    call string_equals
    test rax, rax
    jnz .successful
    sub r12, SHIFT
    mov r12, [r12]
    test r12, r12
    jz .end
    jmp .loop
.successful:
    pop r13
    pop r12
    sub r12, SHIFT
    mov rax, r12
    ret
.end:
    pop r13
    pop r12
    xor rax, rax
    ret

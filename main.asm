%include "words.inc"
%include "lib.inc"
%include "dict.inc"
global _start

%define BUFFER_SIZE 255
section .bss  
buffer: resb (BUFFER_SIZE + 1)

section .rodata
error_read: db "error, because very long word", 0
not_find_error: db "error, not find word in text", 0

section .text
_start:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE    
    call read_word
    test rax, rax 
    jz .fail
.successful:
    mov rdi, buffer
    mov rsi, first_word
    call find_word
    test rax, rax 
    jz .bad
    add rax, 8
    push rax
    mov rdi, rax
    call string_length
    mov rdi, rax
    pop rax
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    xor rdi, rdi
    call exit
.fail:
    mov rdi, error_read
    call print_string_stderr
    mov rdi, 1
    call exit
.bad:
    mov rdi, not_find_error
    call print_string_stderr
    mov rdi, 1
    call exit
